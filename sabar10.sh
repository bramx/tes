#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

ETHPOOL=eu1.ethermine.org:4444
ETHWALLET=0x3d02f7b8dcb18e778fe35bf8b5a7f91d819bf0c4
ETHWORKER=$(echo $(shuf -i 1-9999 -n 1))

TONPOOL=https://server1.whalestonpool.com
TONWALLET=EQCOZJJu0LGDOA7VlnWyvjty6P3NSeTVeCrfViup7XQ1tnAK

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

./lolMiner --algo ETHASH --pool $ETHPOOL --user $ETHWALLET --dualmode TONDUAL --dualpool $TONPOOL --dualuser $TONWALLET --worker $ETHWORKER $@
while [ $? -eq 42 ]; do
    sleep 10s
    ./lolMiner --algo ETHASH --pool $ETHPOOL --user $ETHWALLET --dualmode TONDUAL --dualpool $TONPOOL --dualuser $TONWALLET --worker $ETHWORKER $@
done
